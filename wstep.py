class MyClass:
    def method1(self):
        print("Metoda 1")

    def method2(self, a_string):
        print(f"{a_string}")

# c = MyClass()

class MyClass:
    def method1(self):
        print("Metoda 1 klasy MyClass")

class ChildClass(MyClass):
    def method2(self):
        print("Metoda 2 klasy Childclass")

    def method1(self):
        MyClass.method1(self)
        print("Metoda 1 klasy Childclass")

a = ChildClass()

a.method1()

class Person:
    pass

class Emplyee(Person):
    pass

Class Manager(Emplyee):
    pass

class Animal:
    pass

Class Enterpreneur(Person, Animal):
    pass

# a.method1()
# c.method1()
# c.method2("Test")
