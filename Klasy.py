import datetime

class Employee:

    _raise_amount = 1.1
    num_of_emps = 0

    def __init__(self, first, last, pay):
        self.first = first
        self.last = last
        self.email = f"{self.first.lower()}.{self.last.lower()}@company.com"
        self.pay = pay
        Employee.num_of_emps =+ 1

    def fullname(self):
        return f"{self.first} {self.last}"

    def apply_raise(self):
        self.pay = self.pay * self.raise_amount

    @classmethod
    def get_company_name(cls):
        print("Nazwa firmy")

    @classmethod
    def set_raise_amount(cls, amount):
        cls.__raise_amount = amount

    @staticmethod
    def is_working_day(day):
        if day.weekday() == 5 or day.weekday() == 6:
            return False
        return True


class Developer (Employee):
    _raise_amount = 1.15

    def __init__(self, first, last, pay, prog_lang):
        super().__init__(first, last, pay)
        self.prog_lang = prog_lang


emp1 = Employee("Jan", "Kowalski", "10000")
emp2 = Employee("John", "Doe", "50000")

dev1 = Developer ("Jan", "Doe", 80000, "Python")
dev2 = Developer ("Jan", "v", 30000, "C++")

print(dev1.prog_lang)

print(f"Na początku {Employee.num_of_emps}")
emp1 = Employee("Jan", "Kowalski", "10000")
print(f"Potem {Employee.num_of_emps}")
emp2 = Employee("John", "Doe", "50000")
print(f"Na końcu {Employee.num_of_emps}")

# Employee.set_raise_amount(1.05)
# Employee.raise_amout = 1.05

# emp1.raise_amount = 1.1

# emp1.raise_amount = 1.1
# print(Employee.raise_amount)
# print(emp1.raise_amount)
# print(emp2.raise_amount)

# emp1.get_company_name()
# Employee.get_company_name()

import datetime
my_date = datetime.date(2019, 10, 13)
print(emp1.is_working_day(my_date))

